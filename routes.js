'use strict'
var routeHandlers = require('./routeHandlers')
module.exports = function(){
    return [{
            method: 'GET',
            path: '/',
            config: {
                handler: routeHandlers.landing,
                //description: 'landing-page',
                //notes: 'Routes to landing page',
                //tags: ['api','exampleTag'],
            }
        },
        {
            method: 'GET',
            path: '/hello',
            config: {
                handler: routeHandlers.hello,
                //description: 'helloworld',
                //notes: 'Routes to helloword page',   
            }
        },
        {
            method: 'GET',
            path: '/test',
            config: {
                handler: routeHandlers.test,
                //description: 'helloworld',
                //notes: 'Routes to helloword page',   
            }
        },
        {
            method: 'GET',
            path: '/hellostat',
            config:{
                handler: routeHandlers.hellostat,
                //description: 'helloworld-static',
                //notes: 'Routes to helloworld static page'
            }
        }];
}(); 