var Hapi = require('hapi')
var Good = require('good')
const util = require('util')
var server = new Hapi.Server()
const Joi= require('joi')

const options = {
    ops:{
        interval: 1000
    },
    reporters: {
        console:[{
            module: 'good-squeeze',
            name: 'Squeeze',
            args: [{log: '*', response: '*'}]
        },{
            module: 'good-console'
        },'stdout']
    }
}

server.connection({
    host:'localhost',
    port: '1337',
    //labels: ['web','test','post']
});

server.route({
    method: 'GET',
    path: '/',
    handler: function(request,reply){
        reply('Yes, this page works!');
    }
});

server.route({
    method: 'POST',
    path: '/posttest',
    config:{
       validate:{
            payload: {
                // this is where you need to validate the payload
                callNo: Joi.string().min(1).max(1).required(),
                poNo: Joi.string().max(1),
                cnfrmNo: Joi.string().max(1),
                reqFirstName: Joi.string().max(1),
                reqLastName: Joi.string().max(1),
                reqPhone: Joi.string().max(1),
                inquiryCd: Joi.string().max(1),
                status: Joi.string().max(1),
                responsibility: Joi.string().max(1),
                inquiry: Joi.string().max(1),
                resolution: Joi.string().max(1)
            }
        }
    },
    handler: function(request,reply){
        console.log("we're in the post case");
    }
});
server.register({
    register: require('good'),
    options,
}, (err) => {
    if(err){
        return console.error(err);
    }
    server.start(() => {
    console.log('Server started at: ',server.info.uri)
    });
});


