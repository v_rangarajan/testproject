'use strict'
const Hapi = require('hapi');
const Good = require('good');
const Path = require('path');
const Joi =  require('joi');
const server  = new Hapi.Server();

server.connection({
    host: 'localhost',
    port: 1337,
    labels: ['web']
});

server.route ({
    method: 'GET',
    path: '/',
    handler: function(request,reply){
        reply('Home page');
    }
});
server.route ({
    method: 'GET',
    path: '/notwhatyouaskedfor',
    handler: function(request,reply){
        reply('Go to some other page. This is not what you asked for');
    }
});
server.route({
    method: 'GET',
    path: '/hello/{name?}',
    handler: function(request,reply){
        const user = request.params.name ? encodeURIComponent(request.params.name) : 'world';
        reply('Hello '+user+'!');
    },
    config:{
        validate:{
            params: {
                name:  Joi.string().min(3).max(10)
            }
        }
    }

});
server.route({
    method: 'POST',
    path: '/postit',
    handler: function (request,reply) {
        reply('It\'s posted');
        
    }
});
server.register(require('inert'),(err) => {
    if(err){
        console.error('Failed to load a plugin:',err);
    }

    server.route({
        method: 'GET',
        path: '/hellostat',
        handler: function(request,reply){
            reply.file('./hello.html');
        }
    });

server.register(require('./myplugin'),(err) => {
    if(err){
        console.error('myPlugin issue:',err);
    }

    server.route({
        method: 'GET',
        path: '/plugincheck',
        handler: function(request,reply){
            reply('Plugin entered');
        }
    })
});

    server.route({
        method: 'GET',
        path: '/hello.html',
        handler: function(request,reply){
            reply.file('/hello.html');
        }
    });
});
server.register({
    register: Good,
    options:{
        reporters: {
            console: [{
                module: 'good-squeeze',
                name: 'Squeeze',
                args: [{
                    response: '*',
                    log: '*'
                }]
            },{
                module: 'good-console'
            },'stdout']
        }
    }
},(err) =>{
    if(err){
        throw err;
    }
});
server.ext('onRequest',function(request,reply){
    console.dir("Request: "+request);
    return reply.continue();
});
server.start((err) =>{
    if(err){
        throw err;
    }
    console.log('Server: ',server.info.uri)
});