'use strict'

var landing = function(request,reply){
    reply('Home page');
}
var hello = function(request,reply){
    reply('Hello world');
}
var hellostat = function(request,reply){
    reply.file('./hello.html');
}
var test = function(request,reply){
    reply('Test page');
}
module.exports = {
    landing,
    hello,
    hellostat,
    test
}