var Hapi = require('hapi')
var Good = require('good')
const util = require('util')
var Routes = require('./routes')
var server = new Hapi.Server()

console.log('Imports done')
server.connection({
    host: 'localhost',
    port: 1400
})
console.log('Server defined')
var count = 0;
Routes = Routes.filter(function(x){return x != undefined});
console.log("Routes: ",Routes)

for(var route in Routes){
    server.register(require('inert'),(err) => {
        if(err){
            throw err;
        }
    server.route(Routes[route]);
    count++;
    }
    
    );
}
/*console.log(Routes.length)
for(var route of Routes){
    console.log(util.inspect(route,false,null))
    server.route(JSON.parse(route));
    console.log('Route gen',++count);
}*/

console.log(count+' Routes defined')
server.start((err) => {
    if(err){
        console.log(err)
    }
    console.log('Server: '+server.info.uri)
})
